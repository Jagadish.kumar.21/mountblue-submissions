# FireWall

## Introduction

A firewall is a network-based security system that includes a software program and a piece of hardware that helps to screen out hackers, viruses, and unauthorized access to your data, it warns before when they try to reach your computer and its data.]

The basic task of a firewall is to regulate the flow of traffic between computer networks of different trust levels.
  
## Functions of Firewall

- Network Threat Prevention
- Application and Identity-Based Control
- Hybrid Cloud Support
- Scalable Performance
- Network Traffic Management and Control
- Access Validation
- Record and Report on Events

## Main types of firewalls

1. Packet filtering firewall
2. Circuit-level gateway
3. Application-level gateway (aka proxy firewall)

## Packet filtering firewall

 Packet filtering firewalls operate inline at junction points where devices such as routers and switches do their work. However, these firewalls don't route packets; rather they compare each packet received to a set of established criteria, such as the allowed IP addresses, packet type, port number, and other aspects of the packet protocol headers. Packets that are flagged as troublesome are, generally speaking, unceremoniously dropped -- that is, they are not forwarded and, thus, cease to exist.

 Packet filtering may not provide the level of security necessary for every use case, but there are situations in which this low-cost firewall is a solid option. For small or budget-constrained organizations, packet filtering provides a basic level of security that can protect against known threats. Larger enterprises can also use packet filtering as part of the layered defense to screen potentially harmful traffic between internal departments.

### Advantages

- A single device can filter traffic for the entire network
- Extremely fast and efficient in scanning traffic
- Inexpensive
- Minimal effect on other resources, network performance, and end-user experience

### Disadvantages

- Because traffic filtering is based entirely on IP address or port information, packet filtering lacks broader context that informs other types of firewalls
- Doesn't check the payload and can be easily spoofed
- It is not an ideal option for every network

## Circuit-level gateway

Using another relatively quick way to identify malicious content, circuit-level gateways monitor TCP handshakes and other network protocol session initiation messages across the network as they are established between the local and remote hosts to determine whether the session being initiated is legitimate -- whether the remote system is considered trusted. They don't inspect the packets themselves.  

 While circuit-level gateways provide a higher level of security than packet filtering firewalls, they should be used in conjunction with other systems.

### Advantages

- Only processes requested transactions; all other traffic is rejected
- Easy to set up and manage
- Low cost and minimal impact on end-user experience

### Disadvantages

- If they aren't used in conjunction with other security technology, circuit-level gateways offer no protection against data leakage from devices within the firewall
- No application layer monitoring
- Requires ongoing updates to keep rules current

### Application-level gateway

This kind of device -- technically a proxy and sometimes referred to as a proxy firewall -- functions as the only entry point to and exit point from the network. Application-level gateways filter packets not only according to the service for which they are intended -- as specified by the destination port -- but also by other characteristics, such as the HTTP request string.  

 Application-layer firewalls are best used to protect enterprise resources from web application threats. They can both block access to harmful sites and prevent sensitive information from being leaked from within the firewall. They can, however, introduce a delay in communications

### Advantages

- Examines all communications between outside sources and devices behind the firewall, checking not just address, port, and TCP header information, but the content itself before it lets any traffic pass through the proxy
- Provides fine-grained security controls that can, for example, allow access to a website but restrict which pages on that site the user can open
- Protects user anonymity

### Disadvantages

- Can inhibit network performance
- Costlier than some other firewall options
- Requires a high degree of effort to derive the maximum benefit from the gateway
- Doesn't work with all network protocols

### Limitations of Firewall

When it comes to network security, firewalls are considered the first line of defense. But the question is whether these firewalls are strong enough to make our devices safe from cyber-attacks. The answer may be "no". The best practice is to use a firewall system when using the Internet. However, it is important to use other defense systems to help protect the network and data stored on the computer. Because cyber threats are continually evolving, a firewall should not be the only consideration for protecting the home network.

The importance of using firewalls as a security system is obvious; however, firewalls have some limitations:

- Firewalls cannot stop users from accessing malicious websites, making them vulnerable to internal threats or attacks.
- Firewalls cannot protect against the transfer of virus-infected files or software.
- Firewalls cannot prevent misuse of passwords.
- Firewalls cannot protect if security rules are misconfigured.
- Firewalls cannot protect against non-technical security risks, such as social engineering.
- Firewalls cannot stop or prevent attackers with modems from dialing in to or out of the internal network.
- Firewalls cannot secure the system which is already infected.

## Difference between software Firewall and Hard Firewall

### Software Firewall

A software firewall is a special type of computer software that runs on a computer/server. Its main purpose is to protect your computer/server from outside attempts to control or gain access and depending on your choice of a software firewall. A software firewall can also be configured for checking any suspicious outgoing requests

### Advantages

- It helps block particular sites
- Juniors and parental controls can be supervised
- Ease in maintenance
- Valuable for home users
- Assignment of different levels of access and permissions to the user can be done with ease

### Disadvantages

- Installation and up-gradation are required on individual computers.
- Slow Performance of the system.
- Due to its installation, system resources are consumed.
- Does not work on smart TVs, gaming consoles, etc.

## Hardware Firewall

It is a physical piece of equipment planned to perform firewall duties. A hardware firewall can be a computer or a dedicated piece of equipment that serves as a firewall. Hardware firewalls are incorporated into the router that is situated between the computer and the internet gateway.

### Advantages

- Independently run so less prone to cyber-attacks.
- Installation is external so resources are free from the server.
- Increased bandwidth enables the handling of more data packets per second.
- Reduced latency.
- VPN connection is also supported for increased security and encryption.

### Disadvantages

- Hardware devices can take extra space
- A skilled IT person is required
- Upgradation challenge as it is not cost-effective because multiple devices need to be replaced

## Conclusion

The need for ﬁrewalls has led to their ubiquity. Nearly every organization connected to the Internet has installed some sort of ﬁrewall. The result of this is that most organizations have some level of protection against threats from the outside. Attackers still probe for vulnerabilities that are likely to only apply to machines inside of the ﬁrewall. They also target servers, especially web servers. However, these attackers are to take advantage of the lower security awareness of the home user and get through a VPN connection to the inside of an organization. Hence in this type of situation Firewall helps to halt all action from the attackers or hackers

### References

- <https://www.youtube.com/watch?v=eO6QKDL3p1I&list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6&index=1>
- <https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls>
- <https://www.javatpoint.com/firewall>
- <https://www.geeksforgeeks.org/difference-between-hardware-firewall-and-software-firewall/>
